// Swipe detection

// Prevent scrolling
/*
window.addEventListener('load', function() {
    document.body.addEventListener('touchmove', function(e) {
        e.preventDefault();
    }, false);
}, false);
*/

var obj;
var posX = 0;
var posY = 0;

var newPosX = 0;
var newPosY = 0;
var fingerId = -1;
var startTime = -1;

// Start watching the acceleration 
function startWatch() {
                 
    var previousReading = {
        x: null,
        y: null,
        z: null
    }
	
    counter = 2;
	
//    navigator.accelerometer.watchAcceleration(function (acceleration) {
//        var changes = {},
//        bound = shakeThreshold / 2;
//        if (previousReading.x !== null) {
//            changes.x = Math.abs(previousReading.x - acceleration.x);
//            changes.y = Math.abs(previousReading.y - acceleration.y);
//            changes.z = Math.abs(previousReading.z - acceleration.z);
//        }
//		
//        if (changes.x > bound && changes.y > bound && changes.z > bound && counter == 0) {
//            counter = 5;
//            phoneShake();
//        }
//        else if(counter > 0) {
//            counter--;
//        }
//		
//        previousReading = {
//            x: acceleration.x,
//            y: acceleration.y,
//            z: acceleration.z
//        }
//    }, function() {}, {
//        frequency: 133
//    });
    }

function touchStart(event) {
    console.log("touch start");
    if(event.touches.length != 1)
    {
        fingerId = -1;
        return;
    }
    // If there's exactly one finger inside this element
    var touch = event.targetTouches[0];
    // Place element where the finger is
    posX = touch.screenX;
    posY = touch.screenY;
    fingerId = touch.identifier;
    startTime = new Date().getTime();
}

function touchEnd(event) {
    console.log("touch end");
    var touch = event.changedTouches[0];
    if(touch.identifier != fingerId) return;
	
    // Find out how much the finger moved
    diffX = touch.screenX - posX;
    diffY = touch.screenY - posY;
	
    ratio = Math.abs(diffX / diffY);
    movement = (Math.abs(diffX) + Math.abs(diffY)) / ((screen.width + screen.height) / 100 );
	
    if(ratio > 3 && Math.abs(diffX) > screen.width / 2) {
        if(diffX > 0) {
            swipeLeft();
        }
        else {
            swipeRight();
			console.log("asked swipe right");
        }
    }
    else if(ratio < 0.33 && Math.abs(diffY) > screen.height / 3) {
        if(diffY > 0) {
            swipeUp();
        }
        else {
            swipeDown();
        }
    }
    else if (movement < 2 && (new Date().getTime() < startTime + 300)) {
        screenTap(touch.Target);
    }
    fingerId = -1;
}

function initTouchHandlers() {
    obj = document.getElementById('body');
    obj.addEventListener('touchstart', touchStart, false);
    obj.addEventListener('touchend', touchEnd, false);
    startWatch();
	document.addEventListener("backbutton", onBackKey, false);
        document.addEventListener("menubutton", onMenuButton, false);
    //randomCard();
}

document.addEventListener("deviceready", initTouchHandlers, false);

