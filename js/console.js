Zepto(function($){
    
    var $console = $('<div />', {id: 'Console'}).appendTo($('body'));
    var $titleBar = $('<div>Console</div>', {class:"titlebar"}).appendTo($console);
    var $output = $('<div />', {class:'output'}).appendTo($console);
    var $input = $('<input />', {type: 'text', autocapitalize: 'off', autocorrect: 'off'});
    $('<div />', {class: 'input'}).append($input).appendTo($console);
    
    var animationEnd = 0;
    function repositionConsole() {
        var windowHeight = Math.min(document.documentElement.clientHeight, window.innerHeight || 0);
        var consoleHeight = $console.height();
        var consoleTop = (windowHeight - consoleHeight) + 'px'
        //$console.css('top', consoleTop);
    }
    function updateConsolePosition() {
        repositionConsole();
        var curTime = (new Date()).getTime();
        if(curTime > animationEnd) return;
        
        requestAnimationFrame(updateConsolePosition);
    }
    
    $(window).on('resize', repositionConsole);
    
    
    $titleBar.on('click', function(){
        $console.toggleClass('open');
        if($console.hasClass('open')) {
            $input[0].focus();
        }
        else {
            $input[0].blur();
        }
        
        animationEnd = (new Date()).getTime() + 500;
        updateConsolePosition();
    });
    
    function getValueByStr(str) {
        var parts = str.split(".");
        var context = this;
        for(var i = 0; i < parts.length; i++) {
            var key = parts[i];
            var value;
            if(!key.match(/^[a-zA-Z0-9_$]+$/)) {
                value = function(cmd){
                    return eval.call(context, cmd);
                }.call(context, key);
            }
            else {
                value = context[key];
            }
            
            var type = typeof value;
            if(type === "undefined") return "undefined";
            context = value;
        }
        return context;
    }
    
    $input.on('keydown', function(e){
        if(e.keyCode === 13) {
            try {
                var input = $input.val();
                addToConsole(input, "cmd");
                var valueOfOutput = getValueByStr.call(window, input);
                console.log(valueOfOutput);
            }
            catch(e) {
                console.error("An error occured executing the command");
                console.log(e);
            }
            e.preventDefault();
            $input.val("");
        }
    });
    
    function tryStringify(obj, maxkeys) {
        if(typeof maxkeys !== "number") maxkeys = 7;
        var outputText = obj + '[';
        var keys = Object.keys(obj);
        for(var j = 0; j < keys.length; j++) {
            if(j > 0) outputText += ',';
            var key = keys[j];
            var typeStr = '(' + (typeof key).substring(0, 1) + ')';
            outputText += typeStr + key;

            if(j > maxkeys){
                outputText += ', ...';
                break;
            }
        }
        outputText += ']';
        return outputText;
    }
    
    var originalLog = console.log;
    var originalError = console.error;
    
    function addToConsole(text, cssclass) {
        cssclass = (typeof cssclass === "undefined") ? '' : ' class="' + cssclass + '"';
        var outputText = '<p' + cssclass + '>';
        outputText += text;
        outputText += '</p>';
        $output.append(outputText);
        $output[0].scrollTop = $output[0].scrollHeight;
    }
    
    function getConsoleText() {
        var outputText = "";
        for(var i = 0; i < arguments.length; i++) {
            if(i > 0) outputText += " ";
            var argument = arguments[i];
            var typeofArgument = typeof argument;
            var argumentOutput = "";
            if(typeof argument === "object") {
                try {
                    argumentOutput = JSON.stringify(argument);
                }
                catch(e) {
                    argumentOutput = tryStringify(argument, 7);
                }
            }
            else {
                argumentOutput = argument;
            }
            argumentOutput = '<span class="output-' + typeofArgument + '">' + argumentOutput + '</span>';
            outputText += argumentOutput;
        }
        return outputText;
    }
    
    console.log = function() {
        originalLog.apply(console, arguments);
        var consoleText = getConsoleText.apply(this, arguments);
        addToConsole(consoleText);
    };
    
    console.error = function() {
        originalError.apply(console, arguments);
        var consoleText = getConsoleText.apply(this, arguments);
        addToConsole(consoleText, 'error');
    };
    
    updateConsolePosition();
    window.onerror = function(){
        console.error.apply(this, arguments);
    };

});