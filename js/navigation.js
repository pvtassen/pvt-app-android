function initTouchHandlers() {
    document.addEventListener("backbutton", onBackKey, false);
    document.addEventListener("menubutton", onMenuButton, false);
}
document.addEventListener("deviceready", initTouchHandlers, false);

var currentSubPageUitslagen = 1;
var currentSubPageStanden = 1;
var onpage = "firstlevel";
function swipeRight() {
    //ga naar rechts
    console.log("swipe right");
    if (document.getElementById("uitslagen").style.display == "block") {
        if (currentSubPageUitslagen >= 1 && currentSubPageUitslagen <= 3) {
            document.getElementById("uitslagen" + currentSubPageUitslagen).style.display = "none";
            currentSubPageUitslagen++;
            document.getElementById("uitslagen" + currentSubPageUitslagen).style.display = "block";
        }
    } else if (document.getElementById("standen").style.display == "block") {
        if (currentSubPageStanden >= 1 && currentSubPageStanden <= 3) {
            document.getElementById("standen" + currentSubPageStanden).style.display = "none";
            currentSubPageStanden++;
            document.getElementById("standen" + currentSubPageStanden).style.display = "block";
        }
    }
}
function swipeLeft() {
    //ga naar links
    console.log("swipe left");
    if (document.getElementById("uitslagen").style.display == "block") {
        if (currentSubPageUitslagen >= 2 && currentSubPageUitslagen <= 4) {
            document.getElementById("uitslagen" + currentSubPageUitslagen).style.display = "none";
            currentSubPageUitslagen--;
            document.getElementById("uitslagen" + currentSubPageUitslagen).style.display = "block";
        }
    } else if (document.getElementById("standen").style.display == "block") {
        if (currentSubPageStanden >= 2 && currentSubPageStanden <= 4) {
            document.getElementById("standen" + currentSubPageStanden).style.display = "none";
            currentSubPageStanden--;
            document.getElementById("standen" + currentSubPageStanden).style.display = "block";
        }
    }
}

function onBackKey() {
    console.log("back key");
    // We are going back to home so remove the event listener 
    // so the default back key behaviour will take over
    //document.removeEventListener("backbutton", onBackKey, false);
    //console.log("I've caught a back key");
    if (onPage == "nieuwssub") {
        navigation(1);
    } else if (onPage == "netnieuwssub") {
        navigation(3);
    } else if (onPage == "teamsub") {
        navigation(2);
    } else {
        navigator.app.exitApp();
    }
}
var menuTaps= 0;
function onMenuButton() {
    console.log("menu key" + menuTaps);
    menuTaps++;
    if(menuTaps === 7){
        document.getElementById('Console').style.display = "block";
        alert("Dev modus enabled");
    }
}
function closeOthers() {
    var nodes = document.getElementById('content').childNodes;
    //console.log(nodes);
    for (i = 0; i < nodes.length; i += 1) { 
        nodes[i].style.display = "none";
    }
}
function navigation(x) {
    //console.log(x);
//				if(typeof(Storage)!=="undefined" && !localStorage.password && x == 3){
//					//not logged in
//					//add modal
//					showModal();
//				}else{
    closeOthers();
    if (x === 1) {
        var element = document.getElementById("nieuws");
    }
    if (x === 2) {
        var element = document.getElementById("teams");
    }
    if (x === 3) {
        var element = document.getElementById("netnieuws");
    }
    if (x === 4) {
        var element = document.getElementById("uitslagen");
    }
    if (x === 5) {
        var element = document.getElementById("standen");
    }
    onPage = "firstlevel";
    element.style.display = "block";
//				}
}

function goToElement(type, id) {
    closeOthers();
    //console.log(id);
    if (type == "nieuws") {
        document.getElementById('nieuwsitem' + id).style.display = "block";
        onPage = "nieuwssub";
    }
    if (type == "netnieuws") {
        document.getElementById('netnieuwsitem' + id).style.display = "block";
        onPage = "netnieuwssub";
    }
    if (type == "team") {
        document.getElementById('team' + id).style.display = "block";
        onPage = "teamsub";
    }
}